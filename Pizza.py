class Pizza(object):

    def __init__(self):
        self.size = None
        self.crust_type = None
        self.cheese_type = None
        self.toppings = []
        self.cost = 0


    def to_string(self):
        string = 'A {size} pizza with {cheese} and {toppings} that costs ${cost}'.format(size=self.size,
                                                                            toppings=", ".join(self.toppings),
                                                                            cost=self.cost)
        return string

    def set_crust_type(self, crust):
        self.crust_type = crust

    def set_size(self, size):
        self.size = size

    #def set_cheese_type(self, cheese):
        self.cheese_type = cheese

    def get_size(self):
        return self.size

    def get_cost(self):
        return self.cost

    def add_topping(self, topping):
        self.toppings.append(topping)

    def add_price(self, price):
        self.cost = self.cost + price
