from Pizza import Pizza


#Function to select the price of your pizza object
def pizzaSelection(pizza_obj):
    size = {'SMALL': 5.00,
            'MEDIUM': 8.00,
            'LARGE': 10.00,
            'XL': 12.00}
#Creates a dictionary with the associated Size(key) and price(value).

    validchoice = False
    while validchoice is False:
        print('What size of pizza would you like? ')
        choice = input('Small, Medium, Large or XL? ').upper()
        print (choice, '\n')
#Recieves User's input for their size of pizza

        try:
            price = size[choice]
            validchoice = True
            pizza_obj.add_price(price)
            pizza_obj.set_size(choice)
        except KeyError:
            print ('That is an invalid choice, please try again.','\n')
    pizza_obj = crustSelection(pizza_obj)
    return pizza_obj
#Catches any errors from user input and directs them to the beginning of the loop


#Function to select the crust for the pizza object
def crustSelection(pizza_obj):
    crust = ['ORIGINAL', 'THIN', 'PAN']
#Creates a list with the types of crust

    validchoice = False
    while validchoice is False:
        print('What type of crust would you like?')
        choice = input('Original, Thin or Pan? ').upper()
        if choice in crust:
            print ('Good Choice', '\n')
            pizza_obj.set_crust_type(choice)
            validchoice = True
        else:
            print ('That is an invalid choice, please try again.')
#allows the user to select their type of crust while their choice is in crust
#if choice isn't in curst it will prompt them to start this loop over

    pizza_obj = toppingSelection(pizza_obj)
    return pizza_obj



#Function created for toppic selection of pizza object
def toppingSelection(pizza_obj):
    topping_multiplier = 0
    size = pizza_obj.get_size()
    if size == 'LARGE':
        topping_multiplier = .50
    if size == 'XL':
        topping_multiplier = 1.00
#Creates a topping_multiplier that will be called upon if the size is 'Large' or 'XL'
#and will add the extra price if selected

    meat_topping_cost = 1.00 + topping_multiplier
    meat_toppings = ['PEPPERONI','SAUASAGE','HAM','CHICKEN','BACON']
    validchoice = False
    while validchoice is False:
        print ('This Pizza\'s current cost is: ', pizza_obj.get_cost(), '\n',
        'What type of meat would you like?, type "Done" when finished'+
        '\n' + 'Each Topping Costs: ', meat_topping_cost , ' dollars.', '\n')
        print (", ".join(meat_toppings), '\n')
#Provides the user the various options for their meat_toppings and will print out
#their current cost of the pizza as you continue to add toppings.

        choice = input().upper()
        if choice == 'DONE':
            validchoice = True
            break
        if choice in meat_toppings:
            pizza_obj.add_topping(choice)
            pizza_obj.add_price(meat_topping_cost)
        else:
            print ('That is an invalid choice, please try again  .')
#Provides an option for ending a their selection of meats, adds the toppings to the pizza with
#the updated price and will prompt them to start the loop over again if the selection is invalid

    veggie_toppings = {'PEPPERS','OLIVES','TOMATOES','ONIONS','MUSHROOMS'}
    veggie_topping_cost = .50 + topping_multiplier
    validchoice = False
#creates a list of all available veggies and the updated cost multiplyer

    while validchoice is False:
        print ('This Pizza\'s current cost is: ', pizza_obj.get_cost(), '\n',
        'What type of veggie would you like?, type "Done" when finished' +
        '\n' + 'Each Topping Costs: ', veggie_topping_cost , ' dollars.', '\n')
        print (", ".join(veggie_toppings), '\n')

#same thing for the meat topping function just changed for veggies

        choice = input().upper()
        if choice == 'DONE':
            validchoice = True
            break
        if choice in veggie_toppings:
            pizza_obj.add_topping(choice)
            pizza_obj.add_price(veggie_topping_cost)
        else:
            print ('That is an invalid choice, please try again.')
    return pizza_obj


#Prints out a complete list of your order calling on the pizza.to_string() method
def summarizeOrder(current_order):
    for pizza in current_order:
        print (pizza.to_string(), '\n')

#Main function that is the primary facet of the programs UI. Allows them to create multiple
#pizzas and provides them the total for the prices etc.
def Main():
    total_price = 0
    current_order = []
    validchoice = False

    while validchoice is False:
        choice = input('Welcome to Pizza Hut!' '\n'
                       'Type "Pizza" to order a Pizza or "Done" to be Done: ').upper()
        print ('\n')
        if choice == 'DONE':
            validchoice = True
            break
        if choice == 'PIZZA':
            pizza_obj = Pizza()
            pizza_obj = pizzaSelection(pizza_obj)
            current_order.append(pizza_obj)
            total_price = total_price + pizza_obj.get_cost()
            print ('THIS IS OUR CURRENT TOTAL:', total_price, '\n')
        else:
            print ('That is an invalid choice, please try again.')
    summarizeOrder(current_order)
Main()
